package com.string;

public class String2 {
	public static void main(String[] args) {
		String str="Welcome";
		String str1="Welcome";
		String str3=new String("Welcome");
		String str4="Java";
		boolean a = str.equals(str1);
		boolean b = str.equals(str3);
		boolean c = str.equals(str4);
		boolean d = str.equals(str3);
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);//equals() check the contents and not the reference
		
		boolean a1= str==str1;// true
		boolean a2= str==str3;//false
		boolean a3= str==str4;//false
		boolean a4= str==str3;//false
		
		System.out.println(a1);
		System.out.println(a2);
		System.out.println(a3);
		System.out.println(a4);
	}
	

}

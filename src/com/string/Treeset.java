package com.string;

import java.util.Set;
import java.util.TreeSet; 

public class Treeset {
	public static void main(String[] args) {
		Set<Integer> s=new TreeSet<>();
		s.add(60);
		s.add(50);
		s.add(90);
		s.add(40);
		s.add(30);
		s.add(70);
		s.add(10);
		System.out.println(s);
		s.remove(90);
		System.out.println(s);
	}

}

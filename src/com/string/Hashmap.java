package com.string;

import java.util.HashMap;
import java.util.Map;

public class Hashmap {
	public static void main(String[] args) {
		
		Map<Integer, String> m=new HashMap<>();
		m.put(1, "java");
		m.put(2, "selenium");
		m.put(3, "c program");
		m.put(4, "c++");
		System.out.println(m);
		m.remove(3);
		System.out.println(m);
	}

}

package com.string;

public class SringConcept {
	public static void main(String[] args) {
		String s1="Welcome"; //String constant pool area
		char[] c= {'s','e','l','e','n','i','u','m'};
		String s2="Welcome"; //String constant pool area
		String s3=new String("Welcome");// it stored heap memory
		String s4=new String("Hello World");// it stored heap memory
		String s5=new String(c);// it stored heap memory
		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);
		System.out.println(s4);
		System.out.println(s5);
		s1.concat("to java");
		System.out.println(s1);
		s1=s1.concat(" java");
		System.out.println(s1);
	}

}

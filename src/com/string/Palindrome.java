package com.string;

public class Palindrome {
	
	public static void main(String[] args) {
		String str ="Level";
		String rev="";
		for (int i = str.length()-1; i >=0 ; i--) {
			char ch=str.charAt(i);
			rev=rev+ch;
			
		}
		
		if(str.equalsIgnoreCase(rev)) {
			System.out.println("Palindrome");
		}
		else {
			System.out.println("Not Palindrome");
		}
	}

}

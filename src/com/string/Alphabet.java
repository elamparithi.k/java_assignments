package com.string;

public class Alphabet {
	public static void main(String[] args) {
		String s = "Elamparithi";
		char ch = 'l'; 
		int count = 0;
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) == ch)
				count++;
		}
		System.out.println("Character '" + ch + "' appears " + count + " times.");
	}
}

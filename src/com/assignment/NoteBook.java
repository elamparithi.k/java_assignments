package com.assignment;

public class NoteBook extends Book{
	
	public void draw() {  
		System.out.println("draw");
		
	}

	@Override
	public void write() {
		System.out.println("write your code");
		
	}

	@Override
	public void read() {
		System.out.println("read your code");
		
		
	}
	public static void main(String[] args) {
		NoteBook note=new NoteBook();
		note.draw();
		note.write();
		note.read();
	}

}

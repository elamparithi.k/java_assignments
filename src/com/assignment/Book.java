package com.assignment;

public abstract class Book {
	
	public abstract void write();
	public abstract void read();

}
